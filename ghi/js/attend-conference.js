window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        const loadTag = document.getElementById('loading-conference-spinner');
        // Here, add the 'd-none' class to the loading icon
        loadTag.classList.add("d-none");
        // Here, remove the 'd-none' class from the select tag
        selectTag.classList.remove("d-none");

        const formTag = document.getElementById('create-attendee-form'); //Gets the form data and saves it in the formTag variable
        formTag.addEventListener('submit', async event => { //Accesses the formTag element, then listens for submit
            event.preventDefault(); //Prevents the data entered from saving
            const formData = new FormData(formTag); //Gets the data submitted in the form, saves it to the formData variable
            const json = JSON.stringify(Object.fromEntries(formData)); //Converts the data to a Json string
            const attendeesUrl = 'http://localhost:8001/api/attendees/'; //Saves the API url in conferenceURL
            const fetchConfig = { //
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(attendeesUrl, fetchConfig);
            if (response.ok) {
                const successTag = document.getElementById('success-message');
                // Here, add the 'd-none' class to the loading icon
                formTag.classList.add("d-none");
                // Here, remove the 'd-none' class from the select tag
                successTag.classList.remove("d-none");
                formTag.reset();
                const newAttendee = await response.json();
            }
        });
    }

});